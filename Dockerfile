# daemon runs in the background
# run something like tail /var/log/koyuncoind/current to see the status
# be sure to run with volumes, ie:
# docker run -v $(pwd)/koyuncoind:/var/lib/koyuncoind -v $(pwd)/wallet:/home/koyuncoin --rm -ti koyuncoin:0.2.2
ARG base_image_version=0.10.0
FROM phusion/baseimage:$base_image_version

ADD https://github.com/just-containers/s6-overlay/releases/download/v1.21.2.2/s6-overlay-amd64.tar.gz /tmp/
RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C /

ADD https://github.com/just-containers/socklog-overlay/releases/download/v2.1.0-0/socklog-overlay-amd64.tar.gz /tmp/
RUN tar xzf /tmp/socklog-overlay-amd64.tar.gz -C /

ARG koyunCOIN_BRANCH=master
ENV koyunCOIN_BRANCH=${koyunCOIN_BRANCH}

# install build dependencies
# checkout the latest tag
# build and install
RUN apt-get update && \
    apt-get install -y \
      build-essential \
      python-dev \
      gcc-4.9 \
      g++-4.9 \
      git cmake \
      libboost1.58-all-dev && \
    git clone https://github.com/koyuncoin/koyuncoin.git /src/koyuncoin && \
    cd /src/koyuncoin && \
    git checkout $koyunCOIN_BRANCH && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_CXX_FLAGS="-g0 -Os -fPIC -std=gnu++11" .. && \
    make -j$(nproc) && \
    mkdir -p /usr/local/bin && \
    cp src/koyunCoind /usr/local/bin/koyunCoind && \
    cp src/walletd /usr/local/bin/walletd && \
    cp src/zedwallet /usr/local/bin/zedwallet && \
    cp src/miner /usr/local/bin/miner && \
    strip /usr/local/bin/koyunCoind && \
    strip /usr/local/bin/walletd && \
    strip /usr/local/bin/zedwallet && \
    strip /usr/local/bin/miner && \
    cd / && \
    rm -rf /src/koyuncoin && \
    apt-get remove -y build-essential python-dev gcc-4.9 g++-4.9 git cmake libboost1.58-all-dev librocksdb-dev && \
    apt-get autoremove -y && \
    apt-get install -y  \
      libboost-system1.58.0 \
      libboost-filesystem1.58.0 \
      libboost-thread1.58.0 \
      libboost-date-time1.58.0 \
      libboost-chrono1.58.0 \
      libboost-regex1.58.0 \
      libboost-serialization1.58.0 \
      libboost-program-options1.58.0 \
      libicu55

# setup the koyuncoind service
RUN useradd -r -s /usr/sbin/nologin -m -d /var/lib/koyuncoind koyuncoind && \
    useradd -s /bin/bash -m -d /home/koyuncoin koyuncoin && \
    mkdir -p /etc/services.d/koyuncoind/log && \
    mkdir -p /var/log/koyuncoind && \
    echo "#!/usr/bin/execlineb" > /etc/services.d/koyuncoind/run && \
    echo "fdmove -c 2 1" >> /etc/services.d/koyuncoind/run && \
    echo "cd /var/lib/koyuncoind" >> /etc/services.d/koyuncoind/run && \
    echo "export HOME /var/lib/koyuncoind" >> /etc/services.d/koyuncoind/run && \
    echo "s6-setuidgid koyuncoind /usr/local/bin/koyunCoind" >> /etc/services.d/koyuncoind/run && \
    chmod +x /etc/services.d/koyuncoind/run && \
    chown nobody:nogroup /var/log/koyuncoind && \
    echo "#!/usr/bin/execlineb" > /etc/services.d/koyuncoind/log/run && \
    echo "s6-setuidgid nobody" >> /etc/services.d/koyuncoind/log/run && \
    echo "s6-log -bp -- n20 s1000000 /var/log/koyuncoind" >> /etc/services.d/koyuncoind/log/run && \
    chmod +x /etc/services.d/koyuncoind/log/run && \
    echo "/var/lib/koyuncoind true koyuncoind 0644 0755" > /etc/fix-attrs.d/koyuncoind-home && \
    echo "/home/koyuncoin true koyuncoin 0644 0755" > /etc/fix-attrs.d/koyuncoin-home && \
    echo "/var/log/koyuncoind true nobody 0644 0755" > /etc/fix-attrs.d/koyuncoind-logs

VOLUME ["/var/lib/koyuncoind", "/home/koyuncoin","/var/log/koyuncoind"]

ENTRYPOINT ["/init"]
CMD ["/usr/bin/execlineb", "-P", "-c", "emptyenv cd /home/koyuncoin export HOME /home/koyuncoin s6-setuidgid koyuncoin /bin/bash"]
