// Copyright (c) 2018, The koyunCoin Developers
//
// Please see the included LICENSE file for more information

#pragma once

const std::string windowsAsciiArt =
      "\n                                                   \n"
		" _   __                        _____       _       \n"
		"| | / /                       /  __ \     (_)      \n"
		"| |/ /  ___  _   _ _   _ _ __ | /  \/ ___  _ _ __  \n"
		"|    \ / _ \| | | | | | | '_ \| |    / _ \| | '_ \ \n"
		"| |\  \ (_) | |_| | |_| | | | | \__/\ (_) | | | | |\n"
		"\_| \_/\___/ \__, |\__,_|_| |_|\____/\___/|_|_| |_|\n"
		"			  __/ |                                 \n"
		"			 |___/                                  \n";

const std::string nonWindowsAsciiArt = 
      "\n                                                                           \n"
		"██╗  ██╗ ██████╗ ██╗   ██╗██╗   ██╗███╗   ██╗ ██████╗ ██████╗ ██╗███╗   ██╗\n"
		"██║ ██╔╝██╔═══██╗╚██╗ ██╔╝██║   ██║████╗  ██║██╔════╝██╔═══██╗██║████╗  ██║\n"
		"█████╔╝ ██║   ██║ ╚████╔╝ ██║   ██║██╔██╗ ██║██║     ██║   ██║██║██╔██╗ ██║\n"
		"██╔═██╗ ██║   ██║  ╚██╔╝  ██║   ██║██║╚██╗██║██║     ██║   ██║██║██║╚██╗██║\n"
		"██║  ██╗╚██████╔╝   ██║   ╚██████╔╝██║ ╚████║╚██████╗╚██████╔╝██║██║ ╚████║\n"
		"╚═╝  ╚═╝ ╚═════╝    ╚═╝    ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚═════╝ ╚═╝╚═╝  ╚═══╝\n";

/* Windows has some characters it won't display in a terminal. If your ascii
   art works fine on Windows and Linux terminals, just replace 'asciiArt' with
   the art itself, and remove these two #ifdefs and above ascii arts */
#ifdef _WIN32
const std::string asciiArt = windowsAsciiArt;
#else
const std::string asciiArt = nonWindowsAsciiArt;
#endif
