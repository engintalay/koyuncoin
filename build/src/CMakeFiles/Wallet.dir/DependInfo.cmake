# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/Wallet/LegacyKeysImporter.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/Wallet/LegacyKeysImporter.cpp.o"
  "/root/koyuncoin/src/Wallet/WalletAsyncContextCounter.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/Wallet/WalletAsyncContextCounter.cpp.o"
  "/root/koyuncoin/src/Wallet/WalletErrors.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/Wallet/WalletErrors.cpp.o"
  "/root/koyuncoin/src/Wallet/WalletGreen.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/Wallet/WalletGreen.cpp.o"
  "/root/koyuncoin/src/Wallet/WalletSerializationV1.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/Wallet/WalletSerializationV1.cpp.o"
  "/root/koyuncoin/src/Wallet/WalletSerializationV2.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/Wallet/WalletSerializationV2.cpp.o"
  "/root/koyuncoin/src/Wallet/WalletUtils.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/Wallet/WalletUtils.cpp.o"
  "/root/koyuncoin/src/WalletLegacy/KeysStorage.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/WalletLegacy/KeysStorage.cpp.o"
  "/root/koyuncoin/src/WalletLegacy/WalletHelper.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletHelper.cpp.o"
  "/root/koyuncoin/src/WalletLegacy/WalletLegacy.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletLegacy.cpp.o"
  "/root/koyuncoin/src/WalletLegacy/WalletLegacySerialization.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletLegacySerialization.cpp.o"
  "/root/koyuncoin/src/WalletLegacy/WalletLegacySerializer.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletLegacySerializer.cpp.o"
  "/root/koyuncoin/src/WalletLegacy/WalletTransactionSender.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletTransactionSender.cpp.o"
  "/root/koyuncoin/src/WalletLegacy/WalletUnconfirmedTransactions.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletUnconfirmedTransactions.cpp.o"
  "/root/koyuncoin/src/WalletLegacy/WalletUserTransactionsCache.cpp" "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/WalletLegacy/WalletUserTransactionsCache.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/koyuncoin/build/src/CMakeFiles/NodeRpcProxy.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Transfers.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Rpc.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/DependInfo.cmake"
  "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Http.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Serialization.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/CryptoNoteCore.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/System.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Common.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
