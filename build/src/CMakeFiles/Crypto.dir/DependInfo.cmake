# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/root/koyuncoin/src/crypto/aesb.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/aesb.c.o"
  "/root/koyuncoin/src/crypto/blake256.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/blake256.c.o"
  "/root/koyuncoin/src/crypto/chacha8.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/chacha8.c.o"
  "/root/koyuncoin/src/crypto/crypto-ops-data.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/crypto-ops-data.c.o"
  "/root/koyuncoin/src/crypto/crypto-ops.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/crypto-ops.c.o"
  "/root/koyuncoin/src/crypto/groestl.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/groestl.c.o"
  "/root/koyuncoin/src/crypto/hash-extra-blake.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/hash-extra-blake.c.o"
  "/root/koyuncoin/src/crypto/hash-extra-groestl.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/hash-extra-groestl.c.o"
  "/root/koyuncoin/src/crypto/hash-extra-jh.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/hash-extra-jh.c.o"
  "/root/koyuncoin/src/crypto/hash-extra-skein.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/hash-extra-skein.c.o"
  "/root/koyuncoin/src/crypto/hash.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/hash.c.o"
  "/root/koyuncoin/src/crypto/jh.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/jh.c.o"
  "/root/koyuncoin/src/crypto/keccak.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/keccak.c.o"
  "/root/koyuncoin/src/crypto/oaes_lib.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/oaes_lib.c.o"
  "/root/koyuncoin/src/crypto/random.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/random.c.o"
  "/root/koyuncoin/src/crypto/skein.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/skein.c.o"
  "/root/koyuncoin/src/crypto/slow-hash.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/slow-hash.c.o"
  "/root/koyuncoin/src/crypto/tree-hash.c" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/tree-hash.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/crypto/crypto.cpp" "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/crypto/crypto.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
