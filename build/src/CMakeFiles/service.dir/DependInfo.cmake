# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/WalletService/ConfigurationManager.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/ConfigurationManager.cpp.o"
  "/root/koyuncoin/src/WalletService/NodeFactory.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/NodeFactory.cpp.o"
  "/root/koyuncoin/src/WalletService/PaymentGateService.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/PaymentGateService.cpp.o"
  "/root/koyuncoin/src/WalletService/PaymentServiceConfiguration.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/PaymentServiceConfiguration.cpp.o"
  "/root/koyuncoin/src/WalletService/PaymentServiceJsonRpcMessages.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/PaymentServiceJsonRpcMessages.cpp.o"
  "/root/koyuncoin/src/WalletService/PaymentServiceJsonRpcServer.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/PaymentServiceJsonRpcServer.cpp.o"
  "/root/koyuncoin/src/WalletService/RpcNodeConfiguration.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/RpcNodeConfiguration.cpp.o"
  "/root/koyuncoin/src/WalletService/WalletService.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/WalletService.cpp.o"
  "/root/koyuncoin/src/WalletService/WalletServiceErrorCategory.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/WalletServiceErrorCategory.cpp.o"
  "/root/koyuncoin/src/WalletService/main.cpp" "/root/koyuncoin/build/src/CMakeFiles/service.dir/WalletService/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/koyuncoin/build/src/CMakeFiles/WalletService.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/JsonRpcServer.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Wallet.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/NodeRpcProxy.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Transfers.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/CryptoNoteCore.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Rpc.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Http.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/System.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Common.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Mnemonics.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/DependInfo.cmake"
  "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Serialization.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
