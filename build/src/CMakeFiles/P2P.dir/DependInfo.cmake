# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/CryptoNoteProtocol/CryptoNoteProtocolHandler.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/CryptoNoteProtocol/CryptoNoteProtocolHandler.cpp.o"
  "/root/koyuncoin/src/P2p/IP2pNodeInternal.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/IP2pNodeInternal.cpp.o"
  "/root/koyuncoin/src/P2p/LevinProtocol.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/LevinProtocol.cpp.o"
  "/root/koyuncoin/src/P2p/NetNode.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/NetNode.cpp.o"
  "/root/koyuncoin/src/P2p/NetNodeConfig.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/NetNodeConfig.cpp.o"
  "/root/koyuncoin/src/P2p/P2pConnectionProxy.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/P2pConnectionProxy.cpp.o"
  "/root/koyuncoin/src/P2p/P2pContext.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/P2pContext.cpp.o"
  "/root/koyuncoin/src/P2p/P2pContextOwner.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/P2pContextOwner.cpp.o"
  "/root/koyuncoin/src/P2p/P2pInterfaces.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/P2pInterfaces.cpp.o"
  "/root/koyuncoin/src/P2p/P2pNode.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/P2pNode.cpp.o"
  "/root/koyuncoin/src/P2p/P2pNodeConfig.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/P2pNodeConfig.cpp.o"
  "/root/koyuncoin/src/P2p/PeerListManager.cpp" "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/P2p/PeerListManager.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/koyuncoin/build/src/CMakeFiles/CryptoNoteCore.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/DependInfo.cmake"
  "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Rpc.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Common.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Http.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Serialization.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/System.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
