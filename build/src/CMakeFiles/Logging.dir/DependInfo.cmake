# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/Logging/CommonLogger.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/CommonLogger.cpp.o"
  "/root/koyuncoin/src/Logging/ConsoleLogger.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/ConsoleLogger.cpp.o"
  "/root/koyuncoin/src/Logging/FileLogger.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/FileLogger.cpp.o"
  "/root/koyuncoin/src/Logging/ILogger.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/ILogger.cpp.o"
  "/root/koyuncoin/src/Logging/LoggerGroup.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/LoggerGroup.cpp.o"
  "/root/koyuncoin/src/Logging/LoggerManager.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/LoggerManager.cpp.o"
  "/root/koyuncoin/src/Logging/LoggerMessage.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/LoggerMessage.cpp.o"
  "/root/koyuncoin/src/Logging/LoggerRef.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/LoggerRef.cpp.o"
  "/root/koyuncoin/src/Logging/StreamLogger.cpp" "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/Logging/StreamLogger.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
