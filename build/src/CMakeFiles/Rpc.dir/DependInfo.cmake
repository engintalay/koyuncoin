# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/Rpc/HttpClient.cpp" "/root/koyuncoin/build/src/CMakeFiles/Rpc.dir/Rpc/HttpClient.cpp.o"
  "/root/koyuncoin/src/Rpc/HttpServer.cpp" "/root/koyuncoin/build/src/CMakeFiles/Rpc.dir/Rpc/HttpServer.cpp.o"
  "/root/koyuncoin/src/Rpc/JsonRpc.cpp" "/root/koyuncoin/build/src/CMakeFiles/Rpc.dir/Rpc/JsonRpc.cpp.o"
  "/root/koyuncoin/src/Rpc/RpcServer.cpp" "/root/koyuncoin/build/src/CMakeFiles/Rpc.dir/Rpc/RpcServer.cpp.o"
  "/root/koyuncoin/src/Rpc/RpcServerConfig.cpp" "/root/koyuncoin/build/src/CMakeFiles/Rpc.dir/Rpc/RpcServerConfig.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/koyuncoin/build/src/CMakeFiles/CryptoNoteCore.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Logging.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/P2P.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Common.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Crypto.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Http.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/Serialization.dir/DependInfo.cmake"
  "/root/koyuncoin/build/src/CMakeFiles/System.dir/DependInfo.cmake"
  "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
