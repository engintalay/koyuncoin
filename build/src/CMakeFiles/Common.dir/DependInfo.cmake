# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/Common/Base58.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/Base58.cpp.o"
  "/root/koyuncoin/src/Common/BlockingQueue.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/BlockingQueue.cpp.o"
  "/root/koyuncoin/src/Common/CommandLine.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/CommandLine.cpp.o"
  "/root/koyuncoin/src/Common/ConsoleHandler.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/ConsoleHandler.cpp.o"
  "/root/koyuncoin/src/Common/ConsoleTools.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/ConsoleTools.cpp.o"
  "/root/koyuncoin/src/Common/FileMappedVector.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/FileMappedVector.cpp.o"
  "/root/koyuncoin/src/Common/FormatTools.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/FormatTools.cpp.o"
  "/root/koyuncoin/src/Common/IInputStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/IInputStream.cpp.o"
  "/root/koyuncoin/src/Common/IOutputStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/IOutputStream.cpp.o"
  "/root/koyuncoin/src/Common/JsonValue.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/JsonValue.cpp.o"
  "/root/koyuncoin/src/Common/Math.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/Math.cpp.o"
  "/root/koyuncoin/src/Common/MemoryInputStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/MemoryInputStream.cpp.o"
  "/root/koyuncoin/src/Common/PathTools.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/PathTools.cpp.o"
  "/root/koyuncoin/src/Common/ScopeExit.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/ScopeExit.cpp.o"
  "/root/koyuncoin/src/Common/SignalHandler.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/SignalHandler.cpp.o"
  "/root/koyuncoin/src/Common/StdInputStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/StdInputStream.cpp.o"
  "/root/koyuncoin/src/Common/StdOutputStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/StdOutputStream.cpp.o"
  "/root/koyuncoin/src/Common/StreamTools.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/StreamTools.cpp.o"
  "/root/koyuncoin/src/Common/StringInputStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/StringInputStream.cpp.o"
  "/root/koyuncoin/src/Common/StringOutputStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/StringOutputStream.cpp.o"
  "/root/koyuncoin/src/Common/StringTools.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/StringTools.cpp.o"
  "/root/koyuncoin/src/Common/StringView.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/StringView.cpp.o"
  "/root/koyuncoin/src/Common/Util.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/Util.cpp.o"
  "/root/koyuncoin/src/Common/VectorOutputStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/Common.dir/Common/VectorOutputStream.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
