# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/Platform/Linux/System/Dispatcher.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/Platform/Linux/System/Dispatcher.cpp.o"
  "/root/koyuncoin/src/Platform/Linux/System/ErrorMessage.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/Platform/Linux/System/ErrorMessage.cpp.o"
  "/root/koyuncoin/src/Platform/Linux/System/Ipv4Resolver.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/Platform/Linux/System/Ipv4Resolver.cpp.o"
  "/root/koyuncoin/src/Platform/Linux/System/TcpConnection.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/Platform/Linux/System/TcpConnection.cpp.o"
  "/root/koyuncoin/src/Platform/Linux/System/TcpConnector.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/Platform/Linux/System/TcpConnector.cpp.o"
  "/root/koyuncoin/src/Platform/Linux/System/TcpListener.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/Platform/Linux/System/TcpListener.cpp.o"
  "/root/koyuncoin/src/Platform/Linux/System/Timer.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/Platform/Linux/System/Timer.cpp.o"
  "/root/koyuncoin/src/Platform/Posix/System/MemoryMappedFile.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/Platform/Posix/System/MemoryMappedFile.cpp.o"
  "/root/koyuncoin/src/System/ContextGroup.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/System/ContextGroup.cpp.o"
  "/root/koyuncoin/src/System/ContextGroupTimeout.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/System/ContextGroupTimeout.cpp.o"
  "/root/koyuncoin/src/System/Event.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/System/Event.cpp.o"
  "/root/koyuncoin/src/System/EventLock.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/System/EventLock.cpp.o"
  "/root/koyuncoin/src/System/InterruptedException.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/System/InterruptedException.cpp.o"
  "/root/koyuncoin/src/System/Ipv4Address.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/System/Ipv4Address.cpp.o"
  "/root/koyuncoin/src/System/RemoteEventLock.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/System/RemoteEventLock.cpp.o"
  "/root/koyuncoin/src/System/TcpStream.cpp" "/root/koyuncoin/build/src/CMakeFiles/System.dir/System/TcpStream.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
