# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/root/koyuncoin/src/Transfers/BlockchainSynchronizer.cpp" "/root/koyuncoin/build/src/CMakeFiles/Transfers.dir/Transfers/BlockchainSynchronizer.cpp.o"
  "/root/koyuncoin/src/Transfers/SynchronizationState.cpp" "/root/koyuncoin/build/src/CMakeFiles/Transfers.dir/Transfers/SynchronizationState.cpp.o"
  "/root/koyuncoin/src/Transfers/TransfersConsumer.cpp" "/root/koyuncoin/build/src/CMakeFiles/Transfers.dir/Transfers/TransfersConsumer.cpp.o"
  "/root/koyuncoin/src/Transfers/TransfersContainer.cpp" "/root/koyuncoin/build/src/CMakeFiles/Transfers.dir/Transfers/TransfersContainer.cpp.o"
  "/root/koyuncoin/src/Transfers/TransfersSubscription.cpp" "/root/koyuncoin/build/src/CMakeFiles/Transfers.dir/Transfers/TransfersSubscription.cpp.o"
  "/root/koyuncoin/src/Transfers/TransfersSynchronizer.cpp" "/root/koyuncoin/build/src/CMakeFiles/Transfers.dir/Transfers/TransfersSynchronizer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "STATICLIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "../external/rocksdb/include"
  "../external/linenoise"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
