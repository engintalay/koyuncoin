# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/root/koyuncoin/external/miniupnpc/connecthostport.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/connecthostport.c.o"
  "/root/koyuncoin/external/miniupnpc/igd_desc_parse.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/igd_desc_parse.c.o"
  "/root/koyuncoin/external/miniupnpc/minisoap.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/minisoap.c.o"
  "/root/koyuncoin/external/miniupnpc/minissdpc.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/minissdpc.c.o"
  "/root/koyuncoin/external/miniupnpc/miniupnpc.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/miniupnpc.c.o"
  "/root/koyuncoin/external/miniupnpc/miniwget.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/miniwget.c.o"
  "/root/koyuncoin/external/miniupnpc/minixml.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/minixml.c.o"
  "/root/koyuncoin/external/miniupnpc/portlistingparse.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/portlistingparse.c.o"
  "/root/koyuncoin/external/miniupnpc/receivedata.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/receivedata.c.o"
  "/root/koyuncoin/external/miniupnpc/upnpc.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/upnpc.c.o"
  "/root/koyuncoin/external/miniupnpc/upnpcommands.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/upnpcommands.c.o"
  "/root/koyuncoin/external/miniupnpc/upnperrors.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/upnperrors.c.o"
  "/root/koyuncoin/external/miniupnpc/upnpreplyparse.c" "/root/koyuncoin/build/external/miniupnpc/CMakeFiles/upnpc-static.dir/upnpreplyparse.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "MINIUPNPC_SET_SOCKET_TIMEOUT"
  "_BSD_SOURCE"
  "_POSIX_C_SOURCE=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "version"
  "../src"
  "../external"
  "../src/Platform/Linux"
  "../src/Platform/Posix"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
